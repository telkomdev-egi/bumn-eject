import React, { Component } from "react";
import Login from "./login.js";
import Register from "./register.js";
import Beranda from "./HomeScreen.js";
import LoginProfil from "./loginProfil.js";
import Riwayat from "./Riwayat.js";
import Bantuan from "./bantuan.js";
import Akun from "./akun.js";
import BerandaDetail from "./berandaDetail.js";
import Photo from "../photo/index.js";
import DeskripsiEvidance from "./deskripsiEvidence.js";



import { StackNavigator } from "react-navigation";
export default (DrawNav = StackNavigator({
	Login : {screen : Login ,
	headerMode: 'none',
    header: null,
    navigationOptions: {
        header: null
    }},
    LoginProfil : {screen : LoginProfil ,
        headerMode: 'none',
        header: null,
        navigationOptions: {
            header: null
        }},
    Beranda : {screen : Beranda ,
        headerMode: 'none',
        header: null,
        navigationOptions: {
            header: null
        }},
         Riwayat : {screen : Riwayat ,
        headerMode: 'none',
        header: null,
        navigationOptions: {
            header: null
        }},
        Bantuan : {screen : Bantuan ,
        headerMode: 'none',
        header: null,
        navigationOptions: {
            header: null
        }},
        Akun : {screen : Akun ,
        headerMode: 'none',
        header: null,
        navigationOptions: {
            header: null
        }},
        BerandaDetail : {screen : BerandaDetail ,
        headerMode: 'none',
        header: null,
        navigationOptions: {
            header: null
        }},
        Photo : {screen : Photo ,
        headerMode: 'none',
        header: null,
        navigationOptions: {
            header: null
        }},
        DeskripsiEvidance : {screen : DeskripsiEvidance ,
        headerMode: 'none',
        header: null,
        navigationOptions: {
            header: null
        }},
 
}));